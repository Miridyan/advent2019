#![deny(rust_2018_idioms)]

use std::cmp;
use std::fmt;
use std::fs::File;
use std::i32;
use std::io::prelude::*;

/// Structure represeting a path build from instructions.
#[derive(Debug)]
pub struct Path {
    pub p: Vec<Point>,
}

/// Structure represeting a point in space. This is effectively no different than (i32, i32)
/// except it allows me to implement functions on it like `max()` and `manhattan_distance()`
#[derive(Clone, Debug)]
pub struct Point(i32, i32);

/// Functions for `Path`
impl Path {
    /// Take a Vec<&str> of instructions, covert it to a direction (U, D, L, R) and
    /// a magnitude then build a path from the instructions.
    pub fn new(input: Vec<&str>) -> Path {
        let mut path = vec![Point(0, 0)];

        for instr in input {
            let mut iter = instr.chars();

            let dir = iter.next().unwrap();
            let mag = iter.collect::<String>().parse::<i32>().unwrap();

            let last = path.last().unwrap().clone();
            let (prev_x, prev_y) = (last.0, last.1);

            match dir {
                'U' => path.push(Point(prev_x, prev_y + mag)),
                'D' => path.push(Point(prev_x, prev_y - mag)),
                'L' => path.push(Point(prev_x - mag, prev_y)),
                'R' => path.push(Point(prev_x + mag, prev_y)),
                _ => panic!("Unknown direction"),
            }
        }

        Path { p: path }
    }

    /// Given some `Path`, find all the intersections between line segments.
    /// This method uses the helper function `intersects`.
    pub fn intersections(&self, path: &Path) -> Vec<Point> {
        let mut intersections = Vec::new();

        for i in 0..self.p.len() - 1 {
            for j in 0..path.p.len() - 1 {
                match intersects(&self.p[i], &self.p[i + 1], &path.p[j], &path.p[j + 1]) {
                    Some(p) => intersections.push(p),
                    None => (),
                }
            }
        }

        intersections
    }
}

/// Functions for `Point`
impl Point {
    /// Return the maximum possible point. This is purely for use in the `fold()` function.
    pub fn max() -> Point {
        Point(i32::MAX, i32::MAX)
    }

    /// Find the manhattan distance of a point (just x + y, not actually the hypotenuse)
    pub fn manhattan_distance(&self) -> i32 {
        self.0.abs() + self.1.abs()
    }
}

/// Formatter for `Point`
impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.0, self.1)
    }
}

/// P1 & P2 are the start and stop of line 1. P3 & P4 are the start and stop of line 2.
fn intersects(p1: &Point, p2: &Point, p3: &Point, p4: &Point) -> Option<Point> {
    if p1.0 == p2.0 && p3.1 == p4.1 {
        if p3.1 < cmp::max(p1.1, p2.1)
            && p3.1 > cmp::min(p1.1, p2.1)
            && p1.0 < cmp::max(p3.0, p4.0)
            && p1.0 > cmp::min(p3.0, p4.0)
        {
            return Some(Point(p1.0, p3.1));
        } else {
            return None;
        }
    } else if p3.0 == p4.0 && p1.1 == p2.1 {
        return intersects(p3, p4, p1, p2);
    }

    None
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("input/day3.txt")?;
    let mut contents = String::new();

    file.read_to_string(&mut contents)?;

    let paths: Vec<Path> = contents
        .lines()
        .map(|line| Path::new(line.split(",").collect::<Vec<&str>>()))
        .collect();

    let intersections = paths[0].intersections(&paths[1]);
    let dists = intersections.iter().map(|point| point.manhattan_distance());

    let closest: (Point, i32) =
        intersections
            .iter()
            .zip(dists)
            .fold((Point::max(), i32::MAX), |acc, (point, dist)| {
                if dist == (acc.1).min(dist) {
                    return (point.clone(), dist);
                }
                acc
            });

    println!(
        "The closest intersection is {}, with a distance of {}",
        closest.0, closest.1
    );

    Ok(())
}
