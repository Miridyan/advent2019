package main

import (
    "fmt"
    "io/ioutil"
    "strconv"
    "strings"
)

type IntcodeMachine struct {
    // control flow for machine
    pc   uint
    halt bool
    // contents of machine
    mem  []int
    rom  []int
}

func (i *IntcodeMachine) peek(addr int) int {
    return i.mem[addr]
}

func (i *IntcodeMachine) touch(addr int, val int) {
    i.mem[addr] = val
}

func (i *IntcodeMachine) decode() (int, int, int) {
    return i.mem[i.pc + 1], i.mem[i.pc + 2], i.mem[i.pc + 3]
}

func (i *IntcodeMachine) fde() {
    cmd := i.mem[i.pc]

    switch cmd {
    case 1:
        n1, n2, dest := i.decode()

        i.mem[dest] = i.mem[n1] + i.mem[n2]
        i.pc += 4
    case 2:
        n1, n2, dest := i.decode()

        i.mem[dest] = i.mem[n1] * i.mem[n2]
        i.pc += 4
    case 99:
        i.halt = true
    default:
        fmt.Printf("pc : %v, instr : %v", i.pc, cmd)
        panic("unexpected instruction")
    }
}

func (i *IntcodeMachine) run() {
    for !i.halt {
        i.fde()
    }
}

func (i *IntcodeMachine) reset() {
    i.pc = 0
    i.halt = false
    copy(i.mem, i.rom)
}

func input() []string {
    dat, err := ioutil.ReadFile("input/day2.txt")
    if err != nil {
        panic(err)
    }

    str := string(dat)
    arry := strings.Split(str[0:len(str) - 1], ",")
    return arry
}

func main() {
    input := input()
    data := make([]int, len(input))
    rom := make([]int, len(input))

    for i, item := range input {
        n, err := strconv.ParseInt(item, 10, 32)
        if err != nil {
            panic(err)
        }

        data[i] = int(n)
    }

    copy(rom, data)

    machine := IntcodeMachine {
        pc   : 0,
        halt : false,
        mem  : data,
        rom  : rom,
    }

    machine.touch(1, 12)
    machine.touch(2, 2)
    machine.run()

    fmt.Printf("The result at addr 0 : %v\n", machine.peek(0))

    for i := 0; i <= 99; i++ {
        for j := 0; j <= 99; j++ {
            machine.reset()
            machine.touch(1, i)
            machine.touch(2, j)
            machine.run()

            if machine.peek(0) == 19690720 {
                fmt.Printf("Noun : %v, Verb : %v\n", i, j)
            }
        }
    }
}
